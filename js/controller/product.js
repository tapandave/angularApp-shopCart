(function() {

    var product = function($scope, getProduct, cartser, $cookies) {
        $scope.productItem = [];
        $scope.cartcookie = "";
        getProduct.getData().then(
            function(data) {
                $scope.onSuccess(data)
            },
            function(error) {
                $scope.onError(error)
            });
        $scope.cart = cartser.cart;
        $scope.buynow = function(item) {

            var existingItem = getExistingCartItem(item.id);

            if (existingItem == null) {

                $scope.cart.push({ name: item.name, quantity: 1, id: item.id, price: item.price, imageUrl: item.imageUrl, desc: item.desc })
                cartcookie = $cookies.putObject('cookieStore', $scope.cart);

            } else {

                existingItem.quantity++;
                cartcookie = $cookies.putObject('cookieStore', $scope.cart);
            }

        }


        $scope.getCategories = function() {
            var categories = []
            angular.forEach($scope.productItem, function(item) {

                angular.forEach(item.categories, function(category) {
                    if (categories.indexOf(category) == -1) {
                        categories.push(category);
                    }
                })
            });
            return categories;
        }

        function getExistingCartItem(id) {
            for (var i = 0; i < $scope.cart.length; i++) {
                if ($scope.cart[i].id === id) {
                    return $scope.cart[i];
                }
            }
            return null;
        }

        $scope.onSuccess = function(data) {
            angular.copy(data.data, $scope.productItem);
        }

        $scope.onError = function(error) {
            console.log("Request failed " + error);
        }
    }

    angular.module('ecommerceapp').controller('product', product, ['$scope', 'getProduct', 'cartser', '$cookies']);

}());
