(function() {
    
    var cart = function($scope, cartser, $cookies) {

        $scope.cart = cartser.cart;

        $scope.deletecartitem = function(cartitem) {
            $scope.cart.splice($scope.cart.indexOf(cartitem), 1);
            cartcookie = $cookies.putObject('cookieStore', $scope.cart);
        }
        $scope.calcTotal = function() {
            var total = 0;
            for (var i = 0; i < $scope.cart.length; i++) {
                var product = $scope.cart[i];
                total += (product.price * product.quantity);
            }
            return total;
        }
    }

    angular.module('ecommerceapp').controller('cart', cart, ['$scope', 'cartser', '$cookies']);

}());
