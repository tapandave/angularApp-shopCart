(function() {

    angular.module('ecommerceapp').factory('getProduct', ['$http', function($http) {

        var getData = function() {
            return $http.get('https://hidden-reef-26633.herokuapp.com/phones.json');
        }

        return {
            getData: getData

        }

    }]);

    angular.module('ecommerceapp').factory('cartser', ['$cookies', function($cookies) {
        var cookie = $cookies.getObject('cookieStore');

        cart = cookie || [];
        return {
            cart: cart
        }

    }]);
    
    angular.module('ecommerceapp').directive('loading', ['$http', function($http) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                scope.isLoading = function() {
                    return $http.pendingRequests.length > 0;
                };
                scope.$watch(scope.isLoading, function(value) {
                    if (value) {
                        element.removeClass('ng-hide');
                    } else {
                        element.addClass('ng-hide');
                    }
                });
            }
        };
    }]);

    angular.module('ecommerceapp').config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'template/home.html',
                controller: 'product'
            })
            .when('/cart', {
                templateUrl: 'template/cart.html',
                controller: 'cart'
            })
            .otherwise({
                redirectTo: '/'
            });
    });

}());
